#!/bin/sh

mkdir -p $HOME/.local/bin

curl -o $HOME/.local/bin/penta -fsSL "https://gitlab.com/fuocnetwork/penta-go/-/blob/master/build/penta"

chmod +x $HOME/.local/bin/penta

echo "export PATH=$PATH:$HOME/.local/bin" >> $HOME/.zshrc
echo "export PATH=$PATH:$HOME/.local/bin" >> $HOME/.bashrc


echo "Done"

export PATH=$PATH:$HOME/.local/bin

exit

package extra_test

import (
	"reflect"
	"testing"

	"github.com/theckman/yacspin"
	"gitlab.com/fuocnetwork/penta-go/utils/extra"
)

func TestSpinner(t *testing.T) {

  spinner := extra.Spinner()
  var should *yacspin.Spinner
  if reflect.TypeOf(spinner) != reflect.TypeOf(should) {

    t.Fail()
    t.Logf("Incorrect return should be type of %v got %v", reflect.TypeOf(should), reflect.TypeOf(spinner))
  }

}

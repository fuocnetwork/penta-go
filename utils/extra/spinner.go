package extra

import (
	"time"

	"github.com/theckman/yacspin"
)

func Spinner() (*yacspin.Spinner) {

  cfg := yacspin.Config{

    Frequency: 100 * time.Millisecond,
    CharSet: yacspin.CharSets[5],
    Suffix: " penta proccess",
    SuffixAutoColon: true,
    StopCharacter: "✓",
    StopColors: []string{"fgGreen"},

  }

  spinner, _ := yacspin.New(cfg)

  return spinner

}

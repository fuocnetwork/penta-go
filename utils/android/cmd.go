package android

import (
	"bufio"
	"os"
	"os/exec"
	"strings"

	"gitlab.com/fuocnetwork/penta-go/utils"
	"gitlab.com/fuocnetwork/penta-go/utils/extra"
)

func Prepare() {

  solc := checkSolc()
  reader := bufio.NewReader(os.Stdin)
  if !solc {

    n := 0
    for n < 1 {

      utils.YellowText(os.Stdout, "Solidity not found, Install it? (y/n): ")
      text, _ := reader.ReadString('\n')
      text = strings.ReplaceAll(strings.Replace(strings.ToLower(text), "\n", "", -1), " ", "")

      switch text {

      case "y":
        utils.GreenText(os.Stdout, "Installing Solidity compiler\n")

        spinner := extra.Spinner()

        spinner.Colors("green")
        spinner.Start()
        spinner.Message(utils.GreenString("Installing Solidity"))

        installSolc()

        spinner.Suffix(utils.GreenString(" Solidity Installed"))
        spinner.Stop()

        n = 1

      case "n":
        utils.RedText(os.Stdout, "Penta cannot run correctly wothout Solidity\nYou can always install it later.\n")
        os.Exit(1)

      default:
        utils.YellowText(os.Stdout, "Unknown command\n")

      }

    }

  }

  utils.GreenText(os.Stderr, "you ready to go\n")

}

func checkSolc() bool {

  _, err := exec.LookPath("solc")
  if err != nil { return false }
  return true

}

func installSolc() {

  cmd := exec.Command("pkg", "install", "solidity")
  cmd.Run()

}

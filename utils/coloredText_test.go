package utils_test

import (
	"bytes"
	"testing"

	"gitlab.com/fuocnetwork/penta-go/utils"
)

func TestRedText(t *testing.T) {

  text := "Test"
  should := string(utils.ColorRed) + text + string(utils.ColorReset)

  var out bytes.Buffer
  utils.RedText(&out, text)
  if out.String() != should {

    t.Fail()
    t.Logf("Failed %v should be %v", out.String(), should)

  }

}

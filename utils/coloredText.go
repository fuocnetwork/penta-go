package utils

import (
	"fmt"
	"io"
)

var (

  ColorReset = "\033[0m"

  ColorRed = "\033[31m"
  ColorYellow = "\033[33m"
  ColorGreen = "\033[32m"

)

func RedText(w io.Writer, text string) {

  fmt.Fprint(w, string(ColorRed), text, string(ColorReset))

}

func YellowText(w io.Writer, text string) {

  fmt.Fprint(w, string(ColorYellow), text, string(ColorReset))

}

func GreenText(w io.Writer, text string) {

  fmt.Fprint(w, string(ColorGreen), text, string(ColorReset))

}

func GreenString(text string) string {

  return string(ColorGreen) + text + string(ColorReset)

}

func RedString(text string) string {

  return string(ColorRed) + text + string(ColorReset)

}

func YellowString(text string) string {

  return string(ColorYellow) + text + string(ColorReset)

}


